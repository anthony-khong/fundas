def between(xs, low, high):
    return (xs >= low) & (xs <= high)

